#include <iostream>
#include <cmath>



class Vector
{
private:
    double x, y, z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double valueX, double valueY, double valueZ) : x(valueX),y(valueY),z(valueZ)
    {}

    void SetX(double value) {
        x = value;
    }
    double GetX() {
        return x;
    }
        
    void SetY(double value) {
        y = value;
    }
    double GetY() {
        return y;
    }
    
    void SetZ(double value) {
        z = value;
    }
    double GetZ() {
        return z;
    }
    
    double getVectorModule() {
        double vectorModule;
        vectorModule = sqrt( pow(x,2)+ pow(y, 2)+ pow(z, 2));
        return vectorModule;
    }

    void PrintVector() {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }

};

int main()
{
    Vector vectorDefault;
    Vector vectorCustom(3, 5, 7);

    vectorDefault.PrintVector();

    std::cout << vectorCustom.getVectorModule();

}